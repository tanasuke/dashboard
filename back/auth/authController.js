// AuthController.js
var express = require('express');
var router = express.Router();

var User = require('../models/user');
var verifyToken = require('./verifyToken');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');

router.get('/me', verifyToken, function(req, res) {
    User.findById(req.userId, { password: 0 }, function (err, user) {
        if (err)
            return res.status(500).send("There was a problem finding the user.");
        if (!user)
            return res.status(404).send("No user found.");
        
        res.status(200).send(user);
    });
});

router.post('/register', function(req, res) {
  
    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
    
    User.create({
      username : req.body.username,
      password : hashedPassword
    },
    function (err, user) {
      if (err) {
        return res.status(500).send("There was a problem registering the user.")
      }
      // create a token
      var token = jwt.sign({ id: user._id }, config.secret, {
        expiresIn: 86400 // expires in 24 hours
      });
      res.status(200).send({ auth: true, token: token });
    }); 
});

router.post('/login', function(req, res) {
    User.findOne({ username: req.body.username }, function (err, user) {
      if (err)
        return res.status(500).send('Error on the server.');
      if (!user)
        return res.status(404).send('No user found.');
      var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
      if (!passwordIsValid)
        return res.status(401).send({ auth: false, token: null });
      var token = jwt.sign({ id: user._id }, config.secret, {
        expiresIn: 86400 // expires in 24 hours
      });
      res.status(200).send({ auth: true, token: token });
    });
});

router.get('/logout', verifyToken, function(req, res) {
  var newvalues = { $set: { refreshTokenGoogle: '', accessTokenGoogle: '', autologin: '', intraUsername: '', imgurToken: ''}};
  User.updateOne({_id: req.userId}, newvalues, function (err) {
    if (err) {
      res.status(500).json({
        error: "KO: something went wrong."
      })
    }
    else {
      res.status(200).json({
          message: "OK: tokens deleted."
      })
    }
  })
  .catch(err => {
      res.status(500).json({
          error: "KO: user not logged."
      });
  });
});

module.exports = router;